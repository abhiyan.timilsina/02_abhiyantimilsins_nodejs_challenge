//Declaring variable
const fs = require('fs');
const path = require('path');

//Function for Reading file contents
const readFile = filePath=>fs.readFileSync(filePath,'UTF-8');

//Convert to 24 hour Military Time
function convertToMilitary(time) {
 // if(time[0].split(':')[1]==undefined)
 //   time[0]=time[0]+":"+'0';
   
 if(time[1]=='pm')
   {
     let hour;
     if(time[0].split(':')[0]!=12)
      hour =time[0].split(':')[1]==undefined?(parseInt(time[0].split(':')[0])+12)+":"+'00':(parseInt(time[0].split(':')[0])+12)+":"+time[0].split(':')[1];
     else
     hour =time[0].split(':')[1]==undefined?(parseInt(time[0].split(':')[0])-12)+":"+'00':(parseInt(time[0].split(':')[0])-12)+":"+time[0].split(':')[1];
     
     time[0]=hour;
   }
   return time[0];
 }


//Function to get timings from string
const getTimings = (timingString)=>{

  //Initializing variables
   const days = ['Mon' , 'Tue' , 'Wed' , 'Thu' , 'Fri' , 'Sat' , 'Sun'];
   const openingHours = {
     'Mon' : null,
     'Tue' : null,
     'Wed' : null,
     'Thu' : null,
     'Fri' : null,
     'Sat' : null,
     'Sun' :null
   }
   //Breaking string into indivisual timing stings
    var timings = timingString.split('/');
    var openDays=[];
   //Using Regular Expressions for the type Day-Day
   for(let j=0 ; j<timings.length ; j++) {
     
     //Finding Range
     if(/[a-z]{3}-[a-z]{3}/i.test(timings[j])) {
       
       let openDaysRange = timings[j].match(/([a-z]{3})-([a-z]{3})/i);
       for(let k = days.indexOf(openDaysRange[1]) ; k <= days.indexOf(openDaysRange[2]) ; k++ )
        openDays.push(days[k]);
      }
      //Finding single Day
     if(/ [a-z]{3} /i.test(timings[j])) {

       let day = timings[j].match(/\s[a-z]{3}\s/i);
       openDays.push(day[0].trim());
     }

     //Finding Time from string
      let timing = timings[j].match(/(\d\d?:?\d?\d?) ([a-z]{2}) - (\d\d?:?\d?\d?) ([a-z]{2})/i);
      let openTime = convertToMilitary([timing[1],timing[2]]);
      let closeTime = convertToMilitary([timing[3],timing[4]]);

      for(let m=0;m<openDays.length;m++)
      {
        openingHours[openDays[m]] ={
          'openingTime' : openTime,
          'closingTime' : closeTime
        }
      }
      
   }
   return openingHours;
}
//Parse search date
const searchParser = (date) =>{
  var days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
  date = date.split(',');
  [time,notation] = date[2].split(' ');
  time = convertToMilitary([time,notation]);
  date = new Date(date[0]+','+date[1]);
  return [days[date.getDay()-1],time];
}

//Function for parsing the contents
 const parser = (fileContents , searchString )=> {
       //Declaring useful variables
       var entries=[];
       
       var resturants = {};
       
       //Separating file content into indivisual row element
       entries = fileContents.split('\n');
       
       //Removing last newline
       entries = entries.slice(0,entries.length-1);
       
       //Matching various day formats
        for(let i=0 ; i<entries.length ; i++)
         {
           //Seprating resturant names
            let resturantName = entries[i].substring(0,entries[i].indexOf(','));
        
            //Seprating resturan timings
            
            let timings = entries[i].substring(entries[i].indexOf(',')+1);
            
            //Triming quotes in name and timing

            resturantName = resturantName.replace(/(^")|("$)/g, "");
            
            timings = timings.replace(/(^")|("$)/g, "");

            //Adding resturants to object

            resturants[i]={
              'name' : resturantName,
              'timings' : {
                'Mon' : null,
                'Tue' : null,
                'Wed' : null,
                'Thu' : null,
                'Fri' : null,
                'Sat' : null,
                'Sun' :null
              }
            }
            //Parsing opening time from string
           resturants[i].timings = getTimings(timings);
         
                    
   }
   
   // fs.writeFile(path.join(__dirname,'resturant.txt'),JSON.stringify(resturants),(err)=>{
   //   throw err;
   // });
   return resturants;
 }
 const compare = (resturants,searchTime)=>{
    var openResturants = [];
   for(var resturant in resturants) {
     let timingOnGivenDay = resturants[resturant]['timings'][searchTime[0]];
     
     //Continue is resturant is closed
     
     if(timingOnGivenDay==null)
     continue;

     //Check if it is open
     let name = resturants[resturant]['name'];
  
     
     //Time array for calculation
     let time = [0,1,2,3,4,5,6,7,8,9,10,11,11,12,14,15,16,17,18,19,20,21,22,23];
     
     let openHH = parseInt(timingOnGivenDay['openingTime'].split(':')[0]); 
     let closeHH=parseInt(timingOnGivenDay['closingTime'].split(':')[0]);
     let openMM=parseInt(timingOnGivenDay['openingTime'].split(':')[1]);
     let closeMM=parseInt(timingOnGivenDay['closingTime'].split(':')[1]);
     let searchHH=parseInt(searchTime[1].split(':')[0]);
     let searchMM=parseInt(searchTime[1].split(':')[1]);
     
     for(let k = time.indexOf(openHH) ; k <= time.indexOf(closeHH) ; k++)
       {
         if(time[k]===searchHH)
         {
           openResturants.push(name);
         }
       }

}
return [...new Set(openResturants)];
 }
  
 //Diver Function
 function main(){
   //setting up of variable;
    const fileName = process.argv[4];
    const searchString = process.argv[7];
    const filePath = path.join(__dirname,'restaurant.csv');
   
    // const searchString = 'December 21,1999,10:00 pm';
    if(!/[A-Z]{1}[a-z]* \d\d?,\d\d\d\d,\d\d?:?\d?\d? am|pm/.test(searchString))
     {
       console.log("DATE SHOULD BE IN following format : December 21,1996,12:30pm");
       process.exit(0);
     }   
   //Calling Parser Functions
    var resturants =  parser(readFile(filePath),searchString);
    var searchTime =  searchParser(searchString);
  
   //Calling compare functions
    let open = compare(resturants,searchTime);
     for(let j=0 ; j<open.length ;j++)
      console.log(open[j]);  
}

 main();